# CarCar

Team:

* Stesha Carle Automobile Service
* Rysa Zahedul - Automobile Sales

## Design
We will customize our frontend application using Bootstrap and CSS.

## Service microservice

Service contains Technician and Appointment models.  It will integrate with the Inventory microservice to pull the vin through the AutomobileVO model

## Sales microservice

There will be a Salesperson model, Customer model, and Sale model that will be used to implement our API calls. There will be an AutomobileVO model that will interact with the inventory microservice for seamless integration and data exchange.
