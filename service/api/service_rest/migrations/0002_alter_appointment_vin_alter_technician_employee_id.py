# Generated by Django 4.0.3 on 2023-04-24 23:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='vin',
            field=models.CharField(max_length=17),
        ),
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
