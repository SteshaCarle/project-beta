from django.shortcuts import render
from .models import Sale, Customer, Salesperson, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import SalesEncoder, SalespersonEncoder, CustomerEncoder


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def salesperson_detail(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(employee_id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Failed to delete salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def customer_detail(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Failed to delete customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            auto_sold = content["sold"]
            content.pop("sold")

            try:
                employee_id = content["salesperson"]
                salesperson = Salesperson.objects.get(employee_id=employee_id)
                content["salesperson"] = salesperson
            except Salesperson.DoesNotExist:
                response = JsonResponse(
                    {"message": "Invalid employee id"}
                )
                response.status_code = 400
                return response

            try:
                customer_id = content["customer"]
                customer = Customer.objects.get(id=customer_id)
                content["customer"] = customer
            except Customer.DoesNotExist:
                response = JsonResponse(
                    {"message": "Invalid customer id"}
                )
                response.status_code = 400
                return response

            try:
                vin = content["automobile"]
                automobile = AutomobileVO.objects.get(vin=vin)
                content["automobile"] = automobile
            except AutomobileVO.DoesNotExist:
                response = JsonResponse(
                    {"message": "Invalid AutomobileVO"}
                )
                response.status_code = 400
                return response

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create sale"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def sale_detail(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Failed to delete sale"}
            )
            response.status_code = 400
            return response
