import React, { useState, useEffect } from "react";

function AddAppointmentForm() {
  const [technicians, setTechnicians] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    date_time: "",
    reason: "",
    vin: "",
    customer: "",
    technician_id: "",
  });

  const fetchCustomer = async () => {
    const customerUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customerUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchCustomer();
  }, []);

  const handleFormChange = async (event) => {
    const inputName = event.target.name;
    const value = event.target.value;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const apptUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(apptUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        date_time: "",
        reason: "",
        vin: "",
        customer: "",
        technician_id: "",
      });
    }
  };

  const fetchData = async () => {
    const techUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(techUrl);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.customer}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose a Customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.name} value={customer.name}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.date_time}
                placeholder="date_time"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time">Date</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.technician_id}
                required
                name="technician_id"
                id="technician_id"
                className="form-select"
              >
                <option value="">Choose a Technician</option>
                {technicians.map((technician) => (
                  <option key={technician.id} value={technician.id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.reason}
                placeholder="reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>

            <button className="btn btn-primary">Add Appointment</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddAppointmentForm;
