import React, { useState, useEffect } from "react";

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const automobileResponse = await fetch(
      "http://localhost:8100/api/automobiles/"
    );
    if (automobileResponse.ok) {
      const automobileData = await automobileResponse.json();
      setAutomobiles(automobileData.autos);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div>
        <div>
          <h1>Automobiles</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {automobiles.map((auto) => {
                return (
                  <tr key={auto.id}>
                    <td>{auto.vin}</td>
                    <td>{auto.color}</td>
                    <td>{auto.year}</td>
                    <td>{auto.model.name}</td>
                    <td>{auto.model.manufacturer.name}</td>
                    <td>{auto.sold ? "YES" : "NO"}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default AutomobileList;
